package com.test.fdj.dagger.module

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class NetworkModule {

    @Provides
    fun provideMainOkHttClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        return builder.build()
    }
}