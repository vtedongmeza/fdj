package com.test.fdj.dagger.module

import com.test.fdj.presentation.league.LeagueFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    internal abstract fun bindMainFragment(): LeagueFragment
}