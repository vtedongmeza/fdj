package com.test.fdj.dagger.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.test.fdj.dagger.annotation.ViewModelKey
import com.test.fdj.dagger.factory.AppViewModelFactory
import com.test.fdj.presentation.league.LeagueViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LeagueViewModel::class)
    internal abstract fun provideLeagueFragment(leagueViewModel: LeagueViewModel): ViewModel
}