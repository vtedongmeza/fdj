package com.test.fdj.dagger.component

import android.app.Application
import com.test.fdj.FdjApplication
import com.test.fdj.dagger.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        FragmentBindingModule::class,
        ViewModelModule::class,
        NetworkModule::class,
        LeagueModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<DaggerApplication> {
    fun inject(application: FdjApplication)
    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }
}