package com.test.fdj.dagger.module

import com.test.fdj.services.provider.league.ILeagueProvider
import com.test.fdj.services.provider.league.LeagueProvider
import com.test.fdj.services.provider.league.LeaguesApi
import com.test.fdj.services.usecase.ILeagueUseCase
import com.test.fdj.services.usecase.LeagueUseCase
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class LeagueModule {

    @Singleton
    @Provides
    fun provideLeagueProvider(api: LeaguesApi): ILeagueProvider {
        return LeagueProvider(api)
    }

    @Provides
    fun provideLeagueUseCase(leagueProvider: ILeagueProvider): ILeagueUseCase {
        return LeagueUseCase(leagueProvider)
    }

    @Provides
    fun provideLeagueApiService(okHttpClient: OkHttpClient): LeaguesApi {
        return Retrofit.Builder()
            .baseUrl("https://www.thesportsdb.com/api/v1/json/50130162/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(okHttpClient)
            .build().create(LeaguesApi::class.java)
    }
}