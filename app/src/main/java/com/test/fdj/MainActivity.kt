package com.test.fdj

import android.os.Bundle
import com.test.fdj.dagger.factory.AppViewModelFactory
import com.test.fdj.presentation.league.LeagueFragment
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: AppViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, LeagueFragment.newInstance())
                .commitNow()
        }
    }
}