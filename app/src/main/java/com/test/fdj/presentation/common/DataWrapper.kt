package com.test.fdj.presentation.common

sealed class DataWrapper<out T>

data class Loading<out T>(val loading: Boolean) : DataWrapper<T>()
class EmptySuccess<out T> : DataWrapper<T>()
data class Success<out T>(val data: T) : DataWrapper<T>()
data class Failure<out T>(val errorType: String, val throwable: Throwable? = null) :
    DataWrapper<T>()
