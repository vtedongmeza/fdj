package com.test.fdj.presentation

import androidx.lifecycle.ViewModel
import com.test.fdj.MainActivity
import com.test.fdj.dagger.factory.AppViewModelFactory
import com.test.fdj.presentation.extension.vm
import dagger.android.support.DaggerFragment
import kotlin.reflect.KClass

abstract class BaseFragment<T : ViewModel>(private val modelClass: KClass<T>) : DaggerFragment() {

    protected val viewModel: T by lazy { vm(getAppViewModelFactory(), modelClass) }

    private fun getAppViewModelFactory(): AppViewModelFactory =
        (activity as MainActivity).viewModelFactory
}