package com.test.fdj.presentation.extension

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.test.fdj.R
import com.test.fdj.data.Team

fun Team.loadImage(imageView: ImageView) {
    val glideUrl = GlideUrl(strTeamBadge)
    Glide.with(imageView.context).load(glideUrl).fitCenter()
        .into(imageView)
}