package com.test.fdj.presentation.league

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.test.fdj.databinding.FragmentLeagueBinding
import com.test.fdj.presentation.BaseFragment
import com.test.fdj.presentation.common.EmptySuccess
import com.test.fdj.presentation.common.Failure
import com.test.fdj.presentation.common.Loading
import com.test.fdj.presentation.common.Success

class LeagueFragment : BaseFragment<LeagueViewModel>(LeagueViewModel::class) {

    companion object {
        fun newInstance() = LeagueFragment()
    }

    private lateinit var binding: FragmentLeagueBinding

    private val leagueAdapter: LeagueAdapter by lazy {
        LeagueAdapter(requireContext()) {
            Toast.makeText(requireContext(), it.strLeagueAlternate, Toast.LENGTH_LONG).show()
        }
    }

    private lateinit var arrayAdapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLeagueBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getLeagues()
        initView()
        initObservable()
        initListener()
    }

    private fun initListener() {
        binding.searchLeague.addTextChangedListener(
            object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun afterTextChanged(value: Editable?) {
                    viewModel.findLeagueByName(value.toString())?.let {
                        viewModel.getTeamsOfLeague(it.strLeague)
                    }
                }

            }
        )
    }

    private fun initView() {
        with(binding) {
            rvLeagues.layoutManager = GridLayoutManager(requireContext(), 2)
            rvLeagues.adapter = leagueAdapter

            arrayAdapter = ArrayAdapter<String>(
                requireContext(), android.R.layout.simple_list_item_1,
                arrayListOf()
            )
            searchLeague.setAdapter(arrayAdapter)
        }
    }

    private fun initObservable() {
        viewModel.leaguesLiveData.observe(viewLifecycleOwner) { wrapper ->
            when (wrapper) {
                is EmptySuccess -> {
                    Toast.makeText(requireContext(), "No data found", Toast.LENGTH_LONG).show()
                }
                is Failure -> {
                    Toast.makeText(requireContext(), wrapper.errorType, Toast.LENGTH_LONG).show()
                }
                is Loading -> {
                    Toast.makeText(requireContext(), "Load data from server", Toast.LENGTH_LONG)
                        .show()
                }
                is Success -> {
                    Toast.makeText(requireContext(), "Load end successfully", Toast.LENGTH_LONG)
                        .show()
                    arrayAdapter.clear()
                    arrayAdapter.addAll(viewModel.getLeagueNames())
                    arrayAdapter.notifyDataSetChanged()
                }
            }
        }

        viewModel.teamsLiveData.observe(viewLifecycleOwner) { wrapper ->
            when (wrapper) {
                is EmptySuccess -> {
                    Toast.makeText(requireContext(), "No team found", Toast.LENGTH_LONG).show()
                }
                is Failure -> {
                    Toast.makeText(requireContext(), wrapper.errorType, Toast.LENGTH_LONG).show()
                }
                is Loading -> {
                    Toast.makeText(requireContext(), "Load teams from server", Toast.LENGTH_LONG)
                        .show()
                }
                is Success -> {
                    Toast.makeText(
                        requireContext(),
                        "Load teams end successfully",
                        Toast.LENGTH_LONG
                    )
                        .show()
                    leagueAdapter.items = wrapper.data.toMutableList()
                }
            }
        }
    }

}