package com.test.fdj.presentation.league

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.fdj.data.League
import com.test.fdj.data.Team
import com.test.fdj.presentation.common.DataWrapper
import com.test.fdj.presentation.common.Failure
import com.test.fdj.presentation.common.Loading
import com.test.fdj.presentation.common.Success
import com.test.fdj.services.usecase.ILeagueUseCase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class LeagueViewModel @Inject constructor(private val leagueUseCase: ILeagueUseCase) :
    ViewModel() {

    private val subscription: CompositeDisposable by lazy { CompositeDisposable() }
    val leaguesLiveData: MutableLiveData<DataWrapper<Boolean>> by lazy { MutableLiveData() }
    val teamsLiveData: MutableLiveData<DataWrapper<List<Team>>> by lazy { MutableLiveData() }
    private val leagues: MutableList<League> = arrayListOf()


    fun getLeagues() {
        leaguesLiveData.value = Loading(true)
        leagueUseCase.getLeagues().subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                result.sortedBy { it.strLeague }.reversed()
                leaguesLiveData.value = Loading(false)
                leagues.clear()
                leagues.addAll(result)
                leaguesLiveData.postValue(Success(true))
            }, { error ->
                leaguesLiveData.value = Loading(false)
                leaguesLiveData.postValue(Failure("GET LEAD ERROR", error))
            }).autoDispose()

    }

    private fun Disposable.autoDispose() {
        subscription.add(this)
    }

    fun findLeagueByName(code: String): League? {
        return leagues.firstOrNull { it.strLeague.contains(code) }
    }

    /**
     * Load all teams of selected league
     * @param code The name of league
     */
    fun getTeamsOfLeague(leagueName: String) {
        teamsLiveData.value = Loading(true)
        leagueUseCase.getTeams(leagueName).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                teamsLiveData.value = Loading(false)
                teamsLiveData.postValue(Success(result))
            }, { error ->
                teamsLiveData.value = Loading(false)
                teamsLiveData.postValue(Failure("GET LEAD ERROR", error))
            }).autoDispose()
    }

    /**
     * @return List of league names
     */
    fun getLeagueNames(): List<String> {
        return leagues.map { it.strLeague }
    }
}