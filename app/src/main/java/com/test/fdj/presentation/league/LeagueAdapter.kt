package com.test.fdj.presentation.league

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.fdj.data.League
import com.test.fdj.data.Team
import com.test.fdj.databinding.ItemLeagueBinding
import com.test.fdj.presentation.extension.loadImage

class LeagueAdapter(
    private val context: Context,
    private val onItemSelect: (League) -> Unit
) : RecyclerView.Adapter<LeagueAdapter.LeagueViewHolder>() {

    var items: MutableList<Team> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeagueViewHolder {
        return LeagueViewHolder(
            ItemLeagueBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    }

    override fun onBindViewHolder(holder: LeagueViewHolder, position: Int) {
        val item = items[position]
        item.loadImage(holder.binding.ivTeam)
    }

    override fun getItemCount(): Int = items.size

    class LeagueViewHolder(val binding: ItemLeagueBinding) : RecyclerView.ViewHolder(binding.root)
}