package com.test.fdj.data

data class League(
    val idLeague: String,
    val strLeague: String,
    val strLeagueAlternate: String? = null,
    val strSport: String,
    val teams: List<Team> = arrayListOf()
)