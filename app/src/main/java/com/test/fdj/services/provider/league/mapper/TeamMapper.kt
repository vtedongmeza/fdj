package com.test.fdj.services.provider.league.mapper

import com.test.fdj.data.Team
import com.test.fdj.services.provider.league.response.TeamResponse

object TeamMapper {
    fun convertToTeam(input: TeamResponse): List<Team> {
        return if (input.teams.isEmpty()) arrayListOf() else input.teams
            .map { item ->
                Team(
                    item.idAPIfootball,
                    item.idLeague,
                    item.idLeague2,
                    item.idLeague3,
                    item.idLeague4,
                    item.idLeague5,
                    item.idLeague6,
                    item.idLeague7,
                    item.idSoccerXML,
                    item.idTeam,
                    item.intFormedYear,
                    item.intLoved,
                    item.intStadiumCapacity,
                    item.strAlternate,
                    item.strCountry,
                    item.strDescriptionCN,
                    item.strDescriptionDE,
                    item.strDescriptionEN,
                    item.strDescriptionES,
                    item.strDescriptionFR,
                    item.strDescriptionHU,
                    item.strDescriptionIL,
                    item.strDescriptionIT,
                    item.strDescriptionJP,
                    item.strDescriptionNL,
                    item.strDescriptionNO,
                    item.strDescriptionPL,
                    item.strDescriptionPT,
                    item.strDescriptionRU,
                    item.strDescriptionSE,
                    item.strDivision,
                    item.strFacebook,
                    item.strGender,
                    item.strInstagram,
                    item.strKeywords,
                    item.strKitColour1,
                    item.strKitColour2,
                    item.strKitColour3,
                    item.strLeague,
                    item.strLeague2,
                    item.strLeague3,
                    item.strLeague4,
                    item.strLeague5,
                    item.strLeague6,
                    item.strLeague7,
                    item.strLocked,
                    item.strRSS,
                    item.strSport,
                    item.strStadium,
                    item.strStadiumDescription,
                    item.strStadiumLocation,
                    item.strStadiumThumb,
                    item.strTeam,
                    item.strTeamBadge,
                    item.strTeamBanner,
                    item.strTeamFanart1,
                    item.strTeamFanart2,
                    item.strTeamFanart3,
                    item.strTeamFanart4,
                    item.strTeamJersey,
                    item.strTeamLogo,
                    item.strTeamShort,
                    item.strTwitter,
                    item.strWebsite,
                    item.strYoutube
                )
            }
    }
}
