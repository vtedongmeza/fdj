package com.test.fdj.services.provider.league

import com.test.fdj.services.provider.league.response.LeagueResponse
import com.test.fdj.services.provider.league.response.TeamResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface LeaguesApi {

    @GET("all_leagues.php")
    fun getLeagues(): Single<LeagueResponse>

    @GET("search_all_teams.php")
    fun getTeamsFromLeague(@Query("l") leagueCode:String): Single<TeamResponse>
}