package com.test.fdj.services.provider.league

import com.test.fdj.data.League
import com.test.fdj.data.Team
import com.test.fdj.services.provider.league.mapper.LeagueMapper
import com.test.fdj.services.provider.league.mapper.TeamMapper
import io.reactivex.rxjava3.core.Single

class LeagueProvider(private val api: LeaguesApi) : ILeagueProvider {

    override fun getLeagues(): Single<List<League>> {
        return api.getLeagues().map { response ->
            LeagueMapper.convertToLeague(response)
        }
    }

    override fun getTeamFromLeague(league: String): Single<List<Team>> {
        return api.getTeamsFromLeague(league).map { response ->
            TeamMapper.convertToTeam(response)
        }
    }

}

interface ILeagueProvider {
    fun getLeagues(): Single<List<League>>
    fun getTeamFromLeague(league: String): Single<List<Team>>
}