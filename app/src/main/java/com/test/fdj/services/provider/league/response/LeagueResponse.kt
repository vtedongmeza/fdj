package com.test.fdj.services.provider.league.response

class LeagueResponse(val leagues: List<LeagueDto>)

class LeagueDto(
    val idLeague: String,
    val strLeague: String,
    val strLeagueAlternate: String? = null,
    val strSport: String
)