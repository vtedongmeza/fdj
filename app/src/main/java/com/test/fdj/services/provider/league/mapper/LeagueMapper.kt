package com.test.fdj.services.provider.league.mapper

import com.test.fdj.data.League
import com.test.fdj.services.provider.league.response.LeagueResponse

object LeagueMapper {

    fun convertToLeague(input: LeagueResponse): List<League> {
        return if (input.leagues.isEmpty()) arrayListOf() else input.leagues
            .map { item ->
                League(item.idLeague, item.strLeague, item.strLeagueAlternate, item.strSport)
            }
    }
}