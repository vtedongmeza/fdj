package com.test.fdj.services.usecase

import com.test.fdj.data.League
import com.test.fdj.data.Team
import com.test.fdj.services.provider.league.ILeagueProvider
import io.reactivex.rxjava3.core.Single

class LeagueUseCase(private val provider: ILeagueProvider) : ILeagueUseCase {

    override fun getLeagues(): Single<List<League>> {
        return provider.getLeagues()
    }

    override fun getTeams(league: String): Single<List<Team>> {
        return provider.getTeamFromLeague(league)
    }

}

interface ILeagueUseCase {

    fun getLeagues(): Single<List<League>>

    fun getTeams(league: String): Single<List<Team>>
}